//
//  ViewController.swift
//  Pineapel
//  Created by Oscar Sevilla Garduño on 23/07/21.

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet var email: UITextField!
    @IBOutlet var password: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
    }

    @IBAction func loginButton(_ sender: Any) {
        self.navigationController?.pushViewController(WelcomePageViewController(email: "Nikki"), animated: false)
    }
   
}

