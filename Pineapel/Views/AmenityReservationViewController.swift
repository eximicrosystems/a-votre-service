//
//  AmenityReservationViewController.swift
//  Pineapel
//
//  Created by Oscar Sevilla on 23/07/21.
//

import Foundation
import UIKit

class AmenityReservationViewController: UIViewController {
    
    private let email: String
    
    init(email: String) {
        self.email = email
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func feedButton(_ sender: Any) {
        self.navigationController?.pushViewController(FeedViewController(email: "Nikki"), animated: false)
    }
    
    @IBAction func chatButton(_ sender: Any) {
        self.navigationController?.pushViewController(ChatViewController(email: "Nikki"), animated: false)
    }
    
    @IBAction func offeringButton(_ sender: Any) {
        self.navigationController?.pushViewController(OfferingViewController(email: "Nikki"), animated: false)
    }
    
}
