//
//  ChatViewController.swift
//  Pineapel
//
//  Created by Oscar Sevilla on 23/07/21.
//

import Foundation
import UIKit

class ChatViewController: UIViewController {
    
    private let email: String
    @IBOutlet var menu: UIView!
    
    init(email:String) {
        self.email = email
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBAction func menuButton(_ sender: Any) {
        menu.frame.origin.x = 164
    }
    @IBAction func hideMenu(_ sender: Any) {
        menu.frame.origin.x = 428
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func feedButton(_ sender: Any) {
        self.navigationController?.pushViewController(FeedViewController(email: "Nikki"), animated: false)
    }
    @IBAction func offeringButton(_ sender: Any) {
        self.navigationController?.pushViewController(OfferingViewController(email: "Nikki"), animated: false)
    }
    @IBAction func arButton(_ sender: Any){
        self.navigationController?.pushViewController(AmenityReservationViewController(email: "Nikki"), animated: false)
    }
}
