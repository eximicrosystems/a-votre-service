//
//  FeedViewController.swift
//  Pineapel
//
//  Created by Oscar Sevilla Garduño on 23/07/21.
//

import Foundation
import UIKit

class FeedViewController: UIViewController {
    
    private let email: String
    @IBOutlet var backgroud: UIView!
    
    init(email: String) {
        self.email = email
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func menuButton(_ sender: Any) {
        backgroud.frame.origin.x = 164
    }
    
    @IBAction func hideMenu(_ sender: Any) {
        backgroud.frame.origin.x = 428
    }
    
    @IBAction func chatButton(_ sender: Any) {
        self.navigationController?.pushViewController(ChatViewController(email: "Nikki"), animated: false)
    }
    
    @IBAction func offeringButton(_ sender: Any) {
        self.navigationController?.pushViewController(OfferingViewController(email: "Nikki"), animated: false)
    }
    
    @IBAction func arButton(_ sender: Any) {
        self.navigationController?.pushViewController(AmenityReservationViewController(email: "Nikki"), animated: false)
    }
    
}
