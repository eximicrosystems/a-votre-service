//
//  OfferingServiceViewController.swift
//  Pineapel
//
//  Created by Oscar Sevilla on 24/07/21.
//

import Foundation
import UIKit

class OfferingServiceViewController: UIViewController {
    
    private let email: String
    private let pantalla: String
    
    @IBOutlet var menu: UIView!
    @IBOutlet var logo: UIImageView!
    @IBOutlet var producto: UILabel!
    @IBOutlet var costo: UILabel!
    @IBOutlet var descripcion: UILabel!
    
    
    init(email: String, pantalla: String) {
        self.email = email
        self.pantalla = pantalla
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSelector()
    }
    
    func dataSelector() {
        
        switch pantalla {
        case "car":
            logo.image = UIImage(named: "CAR-WASH-ICON-1")
            producto.text = "Classic Small SUV Clean"
            costo.text = "$55 + tax"
            descripcion.text = "Vacuum seating areas and trunk, lite wipe down of seats, dash and consoles, clean cup holders and door panels, polish glass, visor mirrors, rear-view mirror, buff chrome handles and trim. Exterior: Hand wash, lite bug removal, clean wheels and tires, clean wheel wells, condition tires, buff glass, side mirrors and chrome, clean door and trunk jams.\n\nPlease book services at least 24 hours in advance to ensure prompt delivery."
        case "dog":
            logo.image = UIImage(named: "DOG-WALK-ICON-1")
            producto.text = "30 Minute Dog Walk"
            costo.text = "$18 + tax"
            descripcion.text = "Reserve an experienced and reliable dog walker or pet caretaker, so you have more time to enjoy life. Choose between a 30- or 60-minute walk, or 24-hour overnight care at your residence or at the caretaker’s residence.\n\nPlease reserve 24 hours in advance to ensure timely service."
        case "gourmet":
            logo.image = UIImage(named: "GOURMET-CART-ICON-1")
            producto.text = "Classic Sea Salt Nana Chips - 2.7 oz."
            costo.text = "$3.29 + tax"
            descripcion.text = "Kosher, Paleo, Vegan.\n\nMade in: Loveland, CO.\n\nA surprising savory banana that tastes like a potato chip."
        case "help":
            logo.image = UIImage(named: "HELPING-HAND-ICON-1")
            producto.text = "TV Installation Over 60”"
            costo.text = "$150 + tax"
            descripcion.text = "Television installations can be tricky. We are here to simplify. We can install your large screen TV (over 60”) to your preference.\n\nPlease reserve 24 hours in advance, to ensure timely service."
        case "maid":
            logo.image = UIImage(named: "MAID-SERVICE-ICON-1")
            producto.text = "Studio Apartment"
            costo.text = "$90 + tax"
            descripcion.text = "Our professional and efficient team can provide exceptional services to help you keep your home shiny and new . All areas of your home will be expertly cleaned, leaving you with the freedom to focus on other things in your busy world.\n\nPlease reserve 24 hours in advance, to ensure timely service."
        case "chef":
            logo.image = UIImage(named: "PRIVATE-CHEF-ICON-1")
            producto.text = "Private Chef"
            costo.text = "Price On Request"
            descripcion.text = "Sweet living at its finest. Reserve a private chef to provide you, your loved ones, or your friends with a menu offering of your choosing.\n\nPricing is based on materials needed, and the chef’s time. Reserve a time and date, and our concierge will reach out to you with a chef that will sweeten your taste buds. Bon apetit."
        default:
            print("nada")
        }
    }
    
    @IBAction func menuButton(_ sender: Any) {
        menu.frame.origin.x = 164
    }
    
    @IBAction func hideMenuButton(_ sender: Any) {
        menu.frame.origin.x = 428
    }
    
    @IBAction func feedButton(_ sender: Any){
        self.navigationController?.pushViewController(FeedViewController(email: "Nikki"), animated: false)
    }
    @IBAction func chatButton(_ sender: Any){
        self.navigationController?.pushViewController(ChatViewController(email: "Nikki"), animated: false)
    }
    @IBAction func arButton(_ sender: Any){
        self.navigationController?.pushViewController(AmenityReservationViewController(email: "Nikki"), animated: false)
    }
    @IBAction func offeringButton(_ sender: Any){
        self.navigationController?.pushViewController(OfferingViewController(email: "Nikki"), animated: false)
    }
}
