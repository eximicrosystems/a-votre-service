//
//  MenuViewController.swift
//  Pineapel
//
//  Created by Oscar Sevilla Garduño on 23/07/21.
//

import Foundation
import UIKit

class OfferingViewController: UIViewController {
    
    @IBOutlet var menu: UIView!
    private let email: String
    
    init(email: String) {
        self.email = email
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func menuButton(_ sender: Any) {
        menu.frame.origin.x = 164
    }
    
    @IBAction func hideMenu(_ sender: Any) {
        menu.frame.origin.x = 428
    }
    
    @IBAction func carButton(_ sender: Any){
        self.navigationController?.pushViewController(OfferingServiceViewController(email: "Nikki", pantalla: "car"), animated: false)
    }
    
    @IBAction func dogButton(_ sender: Any){
        self.navigationController?.pushViewController(OfferingServiceViewController(email: "Nikki", pantalla: "dog"), animated: false)
    }
    
    @IBAction func gourmetButton(_ sender: Any){
        self.navigationController?.pushViewController(OfferingServiceViewController(email: "Nikki", pantalla: "gourmet"), animated: false)
    }
    
    @IBAction func helpButton(_ sender: Any){
        self.navigationController?.pushViewController(OfferingServiceViewController(email: "Nikki", pantalla: "help"), animated: false)
    }
    
    @IBAction func maidButton(_ sender: Any){
        self.navigationController?.pushViewController(OfferingServiceViewController(email: "Nikki", pantalla: "maid"), animated: false)
    }
    
    @IBAction func chefButton(_ sender: Any){
        self.navigationController?.pushViewController(OfferingServiceViewController(email: "Nikki", pantalla: "chef"), animated: false)
    }
    
    @IBAction func feedButton(_ sender: Any) {
        self.navigationController?.pushViewController(FeedViewController(email: "Nikki"), animated: false)
    }
    
    @IBAction func chatButton(_ sender: Any) {
        self.navigationController?.pushViewController(ChatViewController(email: "Nikki"), animated: false)
    }
    
    @IBAction func arButton(_ sender: Any) {
        self.navigationController?.pushViewController(AmenityReservationViewController(email: "Nikki"), animated: false)
    }
}
