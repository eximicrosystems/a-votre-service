//
//  WelcomePageViewController.swift
//  Pineapel
//
//  Created by Oscar Sevilla Garduño on 23/07/21.
//

import Foundation
import UIKit

class WelcomePageViewController: UIViewController {
    
    private let email: String
    
    init(email:String) {
        self.email = email
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(nextPage), userInfo: nil, repeats: false)
    }
    
    @objc func nextPage(){
        self.navigationController?.pushViewController(FeedViewController(email: "Nikki"), animated: false)
    }
}
